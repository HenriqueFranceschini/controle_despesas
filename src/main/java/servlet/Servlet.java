package servlet;

import DAO.GenericoDAO;
import apoio.Validacoes;
import entidades.Pessoa;
import entidades.Tipo_Lancamento;
import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class Servlet extends HttpServlet {

    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet Servlet</title>");
            out.println("</head>");
            out.println("<body>");
            out.println("<h1>Servlet Servlet at " + request.getContextPath() + "</h1>");
            out.println("</body>");
            out.println("</html>");
        }
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        String param = request.getParameter("param");

        if (param.equals("tela_cadastro_pessoa")) {

            if (request.getParameter("type").equals("cadastro")) {

                request.setAttribute("idInterno", 1);
                encaminharPagina("Tela_Cadastro_Pessoa.jsp", request, response);

            } else if (request.getParameter("type").equals("edit")) {

                int id = Integer.parseInt(String.valueOf(request.getParameter("id")));
                Pessoa pessoa = (Pessoa) GenericoDAO.getObjectBanco(id, Pessoa.class);
                request.setAttribute("idInterno", 2);
                request.setAttribute("pessoa", pessoa);
                encaminharPagina("Tela_Cadastro_Pessoa.jsp", request, response);

            }
        }

        if (param.equals("inativar_Categoria")) {
            int id = Integer.parseInt(request.getParameter("id"));
            Tipo_Lancamento tl = (Tipo_Lancamento) GenericoDAO.getObjectBanco(id, Tipo_Lancamento.class);
            tl.setStatus("I");
            GenericoDAO.inativar(tl);
            response.sendRedirect("Tela_Categoria.jsp");
        }

        if (param.equals("inativar_pessoa")) {
            int id = Integer.parseInt(request.getParameter("id"));
            Pessoa pes = (Pessoa) GenericoDAO.getObjectBanco(id, Pessoa.class);
            pes.setStatus("I");
            GenericoDAO.inativar(pes);
            response.sendRedirect("Tela_Pessoa.jsp");
        }

    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        String param = request.getParameter("param");

        if (param.equals("cadastrar_categoria")) {
            String nome = request.getParameter("name");
            Tipo_Lancamento tl = new Tipo_Lancamento();
            tl.setName(nome);
            tl.setStatus("A");
            if (GenericoDAO.cadastrar(tl)) {
                response.sendRedirect("Tela_Categoria.jsp");
            }
        }

        if (param.equals("salvar_cadastro_pessoa")) {
            if (request.getParameter("id").equals("0")) {
                Pessoa pessoa = new Pessoa();
                pessoa.setName(request.getParameter("name"));
                pessoa.setCpf(request.getParameter("cpf"));
                pessoa.setStatus("A");
                if (GenericoDAO.cadastrar(pessoa)) {
                    request.setAttribute("idInterno", 1);
                    encaminharPagina("Tela_Pessoa.jsp", request, response);
                }
            } else {
                int id = Integer.parseInt(String.valueOf(request.getParameter("id")));
                Pessoa pessoa = (Pessoa) GenericoDAO.getObjectBanco(id, Pessoa.class);
                pessoa.setName(request.getParameter("name"));
                pessoa.setCpf(request.getParameter("cpf"));
                if (GenericoDAO.salvarEdicao(pessoa)) {
                    response.sendRedirect("Tela_Pessoa.jsp");
                }
            }
        }

        if (param.equals("logar")) {
            String usuario = request.getParameter("username");
            String senha = request.getParameter("senha");
            Validacoes val = new Validacoes();
            if (val.logar(usuario, senha)) {
                response.sendRedirect("Tela_Principal.jsp");
            } else {
                response.sendRedirect("index.jsp");
            }
        }
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

    private void encaminharPagina(String pagina, HttpServletRequest request, HttpServletResponse response) {
        try {
            RequestDispatcher rd = request.getRequestDispatcher(pagina);
            rd.forward(request, response);
        } catch (Exception e) {
            System.out.println("Erro ao encaminhar: " + e);
        }
    }
}
