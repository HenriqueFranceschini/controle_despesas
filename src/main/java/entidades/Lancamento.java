package entidades;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name = "lancamento")
public class Lancamento implements Serializable{

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private int id;

    @ManyToOne
    private Usuario usuario;

    @ManyToOne
    private Pessoa pessoa;

    @ManyToOne
    private Tipo_Lancamento tipo_lancamento;

    @Column(name = "value")
    private double value;

    @Column(name = "release_date")
    private String release_date;

    @Column(name = "date_to_paymente")
    private String date_to_payment;

    @Column(name = "payment_day")
    private String payment_date;

    @Column(name = "description")
    private String description;

    @Column(name = "status")
    private String status;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Usuario getUsuario() {
        return usuario;
    }

    public void setUsuario(Usuario usuario) {
        this.usuario = usuario;
    }

    public Pessoa getPessoa() {
        return pessoa;
    }

    public void setPessoa(Pessoa pessoa) {
        this.pessoa = pessoa;
    }

    public Tipo_Lancamento getTipo_lancamento() {
        return tipo_lancamento;
    }

    public void setTipo_lancamento(Tipo_Lancamento tipo_lancamento) {
        this.tipo_lancamento = tipo_lancamento;
    }

    public double getValue() {
        return value;
    }

    public void setValue(double value) {
        this.value = value;
    }

    public String getRelease_date() {
        return release_date;
    }

    public void setRelease_date(String release_date) {
        this.release_date = release_date;
    }

    public String getDate_to_payment() {
        return date_to_payment;
    }

    public void setDate_to_payment(String date_to_payment) {
        this.date_to_payment = date_to_payment;
    }

    public String getPayment_date() {
        return payment_date;
    }

    public void setPayment_date(String payment_date) {
        this.payment_date = payment_date;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

}

