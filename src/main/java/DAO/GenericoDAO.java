package DAO;

import apoio.HibernateUtil;
import entidades.Pessoa;
import entidades.Tipo_Lancamento;
import java.util.ArrayList;
import java.util.List;
import org.hibernate.*;

public class GenericoDAO {

    public static boolean cadastrar(Object obj) {
        Session sessao = null;
        try {
            sessao = HibernateUtil.getSessionFactory().openSession();
            Transaction transacao = sessao.beginTransaction();
            sessao.save(obj);
            transacao.commit();
            return true;
        } catch (Exception e) {
            System.out.println("Erro ao cadastrar: " + e);
            return false;
        } finally {
            sessao.close();
        }
    }

    public static boolean inativar(Object obj) {
        Session sessao = null;
        try {
            sessao = HibernateUtil.getSessionFactory().openSession();
            Transaction transacao = sessao.beginTransaction();
            sessao.update(obj);
            transacao.commit();
            return true;
        } catch (Exception e) {
            System.out.println(e);
            return false;
        } finally {
            sessao.close();
        }
    }

    public static boolean delete(Object obj) {
        Session sessao = null;
        try {
            sessao = HibernateUtil.getSessionFactory().openSession();
            Transaction transacao = sessao.beginTransaction();
            sessao.delete(obj);
            transacao.commit();
            return true;
        } catch (Exception e) {
            System.out.println(e);
            return false;
        } finally {
            sessao.close();
        }
    }
    
    public static Object getObjectBanco(int id, Class classe) {
        Session sessao = HibernateUtil.getSessionFactory().openSession();
        Transaction transacao = sessao.beginTransaction();
        Object obj = null;
        try {
            obj = sessao.get(classe, id);
            transacao.commit();
        } catch (Exception e) {
            System.out.println(e);
        } finally {
            sessao.close();
        }
        return obj;
    }

    public static ArrayList<Tipo_Lancamento> consultarTodasCat() {
        List<Object> resultado = new ArrayList();
        ArrayList<Tipo_Lancamento> tipo_lancamento = new ArrayList();
        Session sessao = null;
        try {
            String sql = "FROM Tipo_Lancamento "
                    + "ORDER BY name";
            sessao = HibernateUtil.getSessionFactory().openSession();
            org.hibernate.Query query = sessao.createQuery(sql);
            resultado = query.list();
            for (int i = 0; i < resultado.size(); i++) {
                Tipo_Lancamento tl = (Tipo_Lancamento) resultado.get(i);
                tipo_lancamento.add(tl);
            }
        } catch (Exception e) {
            System.out.println(e);
        } finally {
            sessao.close();
        }
        return tipo_lancamento;
    }

    public static ArrayList<Pessoa> consultarTodasPes() {
        List<Object> resultado = new ArrayList();
        ArrayList<Pessoa> pessoa = new ArrayList();
        Session sessao = null;
        try {
            String sql = "FROM Pessoa "
                    + "ORDER BY name";
            sessao = HibernateUtil.getSessionFactory().openSession();
            org.hibernate.Query query = sessao.createQuery(sql);
            resultado = query.list();
            for (int i = 0; i < resultado.size(); i++) {
                Pessoa pes = (Pessoa) resultado.get(i);
                pessoa.add(pes);
            }
        } catch (Exception e) {
            System.out.println(e);
        } finally {
            sessao.close();
        }
        return pessoa;
    }

    public static boolean salvarEdicao(Object obj) {
        Session sessao = null;
        try {
            sessao = HibernateUtil.getSessionFactory().openSession();
            Transaction transacao = sessao.beginTransaction();
            sessao.update(obj);
            transacao.commit();
            return true;
        } catch (Exception e) {
            System.out.println(e);
            return false;
        } finally {
            sessao.close();
        }
    }

    public static int getLastId(String classe) {
        Session sessao = null;
        int maiorId = 0;
        try {
            String sql = "SELECT MAX(id) FROM " + classe;
            sessao = HibernateUtil.getSessionFactory().openSession();
            maiorId = Integer.parseInt(sessao.createQuery(sql).list().toString().replaceAll("\\D", ""));
        } catch (Exception e) {
            System.out.println(e);
        } finally {
            sessao.close();
        }
        return maiorId;
    }
}
