--liquibase formatted sql

--changeset henrique:1
CREATE TABLE IF NOT EXISTS usuario(
    id serial PRIMARY KEY,
    username VARCHAR(100) NOT NULL,
    password VARCHAR(50) NOT NULL,
    status VARCHAR(1) NOT NULL
);
--rollback DROP TABLE usuario;


--changeset henrique:2
CREATE TABLE IF NOT EXISTS pessoa(
    id serial PRIMARY KEY,
    name VARCHAR(100) NOT NULL,
    cpf VARCHAR(11) NOT NULL,
    status VARCHAR(1) NOT NULL
);
--rollback DROP TABLE categoria;


--changeset henrique:3
CREATE TABLE IF NOT EXISTS tipo_lancamento(
    id serial PRIMARY KEY,
    name VARCHAR(50) NOT NULL,
    status VARCHAR(1) NOT NULL
);
--rollback DROP TABLE pessoa;


--changeset henrique:4
CREATE TABLE IF NOT EXISTS lancamento(
    id serial PRIMARY KEY,
    user_id INT NOT NULL,
    person_id INT NOT NULL,
    release_type_id INT NOT NULL,
    value DECIMAL(10,3) NOT NULL,
    release_date DATE NOT NULL,
    date_to_payment DATE NOT NULL,
    payment_date DATE,
    description VARCHAR(250), 
    status VARCHAR(1) NOT NULL,
    FOREIGN KEY (user_id) REFERENCES usuario (id),
    FOREIGN KEY (person_id) REFERENCES pessoa (id),
    FOREIGN KEY (release_type_id) REFERENCES tipo_lancamento (id)
);
--rollback DROP TABLE movimento;