<%-- 
    Document   : Tela_Cadastro_Pessoa
    Created on : 29 de mar de 2021, 14:07:18
    Author     : henri
--%>

<%@page import="DAO.GenericoDAO"%>
<%@page import="entidades.Pessoa"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Cadastro de Pessoas</title>
        <style>
            .bd-placeholder-img {
                font-size: 1.125rem;
                text-anchor: middle;
                -webkit-user-select: none;
                -moz-user-select: none;
                user-select: none;
            }

            @media (min-width: 768px) {
                .bd-placeholder-img-lg {
                    font-size: 3.5rem;
                }
            }
        </style>
        
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta.2/css/bootstrap.min.css">

        <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.0/css/all.css">

        <link href="css/bootstrap.min.css" rel="stylesheet" integrity="sha384-eOJMYsd53ii+scO/bJGFsiCZc+5NDVN2yr8+0RDqr0Ql0h+rP48ckxlpbzKgwra6" crossorigin="anonymous">

        <link href="css/navbar.css" rel="stylesheet">   
        
    </head>
    <body>
        <nav class="navbar navbar-expand-md navbar-dark bg-dark" aria-label="Fourth navbar example">
            <div class="container-fluid">
                <h1 class="navbar-brand" href="#">Cadastro de Cliente</h1>
                <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarsExample04" aria-controls="navbarsExample04" aria-expanded="false" aria-label="Toggle navigation">
                    <span class="navbar-toggler-icon"></span>
                </button>

                <div class="collapse navbar-collapse" id="navbarsExample04">
                    <ul class="navbar-nav me-auto mb-2 mb-md-0">     

                        <li class="nav-item">
                            <a class="nav-link active" aria-current="page" href="Tela_Principal.jsp">Dashboard</a>
                        </li>

                        <li class="nav-item dropdown">
                            <a class="nav-link dropdown-toggle" href="#" id="dropdown04" data-bs-toggle="dropdown" aria-expanded="false">Categoria</a>
                            <ul class="dropdown-menu" aria-labelledby="dropdown04">
                                <li><a class="dropdown-item" href="Tela_Cadastro_Categ.jsp">Cadastrar</a></li>
                                <li><a class="dropdown-item" href="Tela_Categoria.jsp">Listar</a></li>
                            </ul>
                        </li>

                        <li class="nav-item dropdown">
                            <a class="nav-link dropdown-toggle" href="#" id="dropdown04" data-bs-toggle="dropdown" aria-expanded="false">Cliente</a>
                            <ul class="dropdown-menu" aria-labelledby="dropdown04">
                                <li><a class="dropdown-item" href="/Controle_Desp/Servlet?param=tela_cadastro_pessoa&type=cadastro">Cadastrar</a></li>
                                <li><a class="dropdown-item" href="Tela_Pessoa.jsp">Listar</a></li>
                            </ul>
                        </li>

                    </ul>

                    <form action="Tela_Pessoa.jsp">
                        <button type="submit" class="btn btn-dark">Voltar</input>
                    </form>

                </div>
            </div>
        </nav>

        <script src="js/bootstrap.bundle.min.js" integrity="sha384-JEW9xMcG8R+pH31jmWH6WWP0WintQrMb4s7ZOdauHnUtxwoG2vI5DkLtS3qm9Ekf" crossorigin="anonymous"></script>

        <%
            Pessoa pessoa = new Pessoa();
            if (request.getAttribute("idInterno").equals(1)) {
                pessoa.setId(0);
                pessoa.setName("");
                pessoa.setCpf("");
            } else {
                pessoa = (Pessoa) request.getAttribute("pessoa");
            }
        %>

        <div id="CadastroUsuario">
            <form name='FormPessoa' id="FormPessoa" method='post' action="/Controle_Desp/Servlet?param=salvar_cadastro_pessoa&id=<%=pessoa.getId()%>">
                <center>
                    
                    <div class="row">
                        <br>
                        <br>
                    </div>

                    <div class="form-group col-md-3">
                        <label for="Nome">Nome Completo</label>
                        <input type="text" class="form-control" id="Nome" name="name" aria-describedby="Nome" placeholder="Digite seu Nome" required="">
                    </div>

                    <div class="form-group col-md-3">
                        <label for="cpf">CPF</label>
                        <input type="text" class="form-control" id="CPF" name="cpf" aria-describedby="CPF" placeholder="Digite seu CPF" required="">
                    </div>

                    <br>
                    
                    <button type="submit" class="btn btn-dark">Salvar</button>
                    
                </center>
            </form>
        </div>

    </body>
</html>
