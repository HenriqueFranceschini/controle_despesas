package testes;

import DAO.GenericoDAO;
import entidades.Pessoa;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runners.MethodSorters;

@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class GenericoDAOTest {

    private Pessoa pessoa = new Pessoa();
    private int lastId = 0;

    @Test
    public void testar1Cadastro() {
        pessoa.setName("Teste");
        pessoa.setCpf("Teste");
        pessoa.setStatus("T");
        assertEquals(true, GenericoDAO.cadastrar(pessoa));
    }

    @Test
    public void testar2GetLastId() {
        lastId = GenericoDAO.getLastId("Pessoa");
        assertNotEquals(0, lastId);
    }

    @Test
    public void testar3GetObject() {
        lastId = GenericoDAO.getLastId("Pessoa");
        pessoa = (Pessoa) GenericoDAO.getObjectBanco(lastId, Pessoa.class);
        assertNotEquals(null, pessoa);
    }

    @Test
    public void testar4Inativar() {
        lastId = GenericoDAO.getLastId("Pessoa");
        pessoa = (Pessoa) GenericoDAO.getObjectBanco(lastId, Pessoa.class);
        pessoa.setStatus("O");
        assertEquals(true, GenericoDAO.inativar(pessoa));
    }

    @Test
    public void testar5TesteInativar() {
        lastId = GenericoDAO.getLastId("Pessoa");
        pessoa = (Pessoa) GenericoDAO.getObjectBanco(lastId, Pessoa.class);
        assertEquals("O", pessoa.getStatus());
    }

    @Test
    public void testar6Delete() {
        lastId = GenericoDAO.getLastId("Pessoa");
        pessoa = (Pessoa) GenericoDAO.getObjectBanco(lastId, Pessoa.class);
        assertEquals(true, GenericoDAO.delete(pessoa));
    }

    @Test
    public void testarGetAllPessoas() {
        assertNotEquals(null, GenericoDAO.consultarTodasPes());
    }
}
